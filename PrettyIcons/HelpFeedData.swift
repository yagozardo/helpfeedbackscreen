/*
 *
 *
 *
 */

import UIKit

class HelpFeedData {
  
  let footer: String?
  var helpFeedArray: [HelpFeed]
  
  init(sectionFooter: String, helpFeedArray: [HelpFeed]) {
    self.footer = sectionFooter
    self.helpFeedArray = helpFeedArray
  }
  
  static func section1() -> HelpFeedData {
    var helpFeedArray = [HelpFeed]()
    helpFeedArray.append(HelpFeed(labelText: "Terms and Conditions"))
    helpFeedArray.append(HelpFeed(labelText: "Send feedback/support"))
        return HelpFeedData(sectionFooter: "If you are contacting us about any technical issue on the app please try to describe the issue", helpFeedArray: helpFeedArray)
  }
    static func section2() -> HelpFeedData {
        var helpFeedArray = [HelpFeed]()
        helpFeedArray.append(HelpFeed(labelText: "Sales"))
        return HelpFeedData(sectionFooter: "Interested in having your mixtape featured on the app? \nGet in touch with our sales team.", helpFeedArray: helpFeedArray)
    }
    static func section3() -> HelpFeedData {
        var helpFeedArray = [HelpFeed]()
        helpFeedArray.append(HelpFeed(labelText: "Digital Millenium Copyright Act."))
        return HelpFeedData(sectionFooter: "If you are a legal representative, contact us.", helpFeedArray: helpFeedArray)
    }

  
  static func helpFeedSet() -> [HelpFeedData] {
    return [HelpFeedData.section1(), HelpFeedData.section2(), HelpFeedData.section3()]
  }
  
}


