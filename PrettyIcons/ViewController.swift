/* 
 *
 *
 */

import UIKit

class ViewController: UIViewController {

  var helpFeed = [HelpFeedData]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    helpFeed = HelpFeedData.helpFeedSet()
    automaticallyAdjustsScrollViewInsets = false
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.barTintColor = UIColor.blackColor()
        nav?.tintColor = UIColor.whiteColor()
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
    }
    
}

extension ViewController : UITableViewDataSource {
  

  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let helpFeedSet =  helpFeed[section]
    return helpFeedSet.helpFeedArray.count
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("TheCell", forIndexPath: indexPath)
    let helpFeedSet = helpFeed[indexPath.section]
    
    let help = helpFeedSet.helpFeedArray[indexPath.row]
    
    cell.textLabel?.text = help.label

    return cell
  }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return helpFeed.count
    }
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        let helpFeedSet = helpFeed[section]
        return helpFeedSet.footer
    }
  
}